# Exercices liés au cours de réseau

Philippe Pary, 30 septembre 2016  
Pour PopSchool Valenciennes http://pop.eu.com  
Licence CC-BY-SA

## Rappel

L’organisation des communications réseau se fait selon un modèle théorique,
le modèle OSI. Ce dernier divise les tâches de communication du niveau le plus
bas (électronique, câble RJ45) au plus haut (protocoles applicatifs comme HTTP)

Ce fichier contient des mini-exercices reprenant la trame générale du cours de
réseau disponible ici : <https://slides.com/philippepary/deck/>

## Niveau applicatif

Ce sont des protocoles texte : tout transite en texte plat. Mais il y en a
beaucoup, vous n’avez pas à tout savoir. Juste comprendre que rien n’est
mystérieux ni géré par des petits lutins (ce qui est fort regrettable)

### DNS et dig

*dig* est un logiciel disponible dans le paquet *dnsutils* sur Debian
($ dnsget -a dans le paquet *udns-utils* est similaire à *dig*)

dig sert à faire des résolution DNS

1. Quelle est l’adresse IP de google.fr ?
2. Quels sont les champs mails (MX) de hotmail.com ?
3. Quelle est l’adresse IPv6 de kame.net ?

### HTTP et telnet

*telnet* est un logiciel (paquet *telnet*) qui permet de se connecter à des
serveurs et à dialoguer avec eux.

Ici, on va aller récupérer la home page du site veille.popschool.fr à la main.

Copiez le texte suivant:
    GET / HTTP/1.1
    Host: veille.popschool.fr

Lancez dans un terminal la commande suivante:
    $ telnet popschool.fr 80

Quand vous avez un curseur, collez le texte.  
Observez bien le résultat, quitte à scroller un peu vers le haut. Vous verrez
d’abord les en-têtes HTTP suivis du code HTML.  
Vous avez un aperçu de comment marche un navigateur web !

Vous pouvez vous ammuser à faire ça avec d’autres pages et d’autres sites webs.

Notez que *telnet* marche pour tous les protocoles applicatifs: SMTP, DNS, FTP…

### Whois

*whois* (paquet *whois*) est un protocole qui dispose d’un logiciel éponyme. Il
permet de savoir à qui appartient un domaine ou une adresse IP

1. Trouvez votre adresse IP publique, faites un WHOIS dessus
2. Faites un whois sur apple.com
3. Faites un whois sur pop.eu.com : Quel est le registrar ? et le propriétaire ?

## IP et TCP/UDP

TCP/IP est le protocole qui permet de prendre le texte des protocole
applicatifs pour les couper en petits morceaux (~1500 caractères à chaque fois)
pour les faire transporter d’un point A à un point B soit sur un réseau local
soit au travers d’Internet

### Attribution d’IP et DHCP

DHCP est un protocole texte qui permet d’attribuer des adresses IP aux machines
d’un réseau à partir d’un point central. Ça évite de devoir configurer les IP
à la main, ça évite de donner la même adresse IP à deux machine

Lancez la commande suivante:
    # dhclient -v

Lancez la plusieurs fois, vous récupérez toujours la même adresse IP. C’est
normal : votre ordinateur commence par demander si l’adresse IP qu’il a eu
avant est toujours disponible.

### ifconfig, route, /etc/resolv.conf

*ifconfig* pour voir sa configuration réseau, *route* ou *route -n* pour voir
sa table de routage, le fichier /etc/resolv.conf contient vos serveurs DNS

Jouez un peu avec tous ça

## Ethernet

On commence à être vers le bas, on est plus très loin de parler matériel.
Ethernet est le protocole des réseaux locaux. Chaque machine est identifié par
le numéro de sa carte réseau, **l’adresse MAC**, qui est un numéro physique
attribué par chaque constructeur.  
**ATTENTION** une adresse MAC peut être modifiée facilement, ce n’est donc pas
un élément possible pour sécuriser sérieusement des accès

### ARP

*arp* pour Addres Resolution Protocol se charge de faire la concordance entre
une adresse IP et une adresse MAC.  
L’adresse MAC est figée sur la carte réseau (matériel) alors que l’IP peut
changer (vous n’avez pas la même à la maison ou à POP !). C’est ARP qui fait
la concordance  
Vous aurez au moins une fois dans votre carrière des problème de cache ARP sur
les routeurs ou sur un serveur, particulièrement dans les endroits où il y a
plein de va-et-vient (cafés, coworkings, openspace …)

1. Tappez la commande suivante pour afficher votre cache ARP
    # arp
2. Pingez une machine du réseau qui n’est pas dans votre cache ARP
3. Refaites la première commande, vous voyez apparaître une nouvelle entrée


## 100-BASE, USB, Wi-Fi, Bluetooth etc.

Ici on parle matériel. On calcule la résistance électrique des câbles,
l’amplitude du signal radio etc.  
On est au niveau matériel quoi.

1. Prenez un câble réseau, regardez ses deux têtes. Est-il droit ?
Est-il croisé ?
2. Si le câble est croisé, faites moi plaisir et jetez le, on ne se sert plus
de ça depuis la fin des années 90 :)

## Resources complémentaires

* « j’ai que 3 minutes à y consacrer » : <http://www.misfu.com/commandes-outils-reseaux-linux.html>
* L’excellent cours de l’École Centrale de Paris : <http://formation-debian.via.ecp.fr/admin-sys.html>
* La bible : <https://raphaelhertzog.fr/livre/cahier-admin-debian/>
