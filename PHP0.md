# PHP, niveau 0

Ce cours va introduire des notions de programmation :

* Variables
* Logique booléenne

## Travailler en PHP

Votre machine virtuelle est prête à recevoir et **interprêter** des scripts PHP

1. Créez un dossier accessible depuis le serveur web
2. Montez le via Nautilus (il vous sera plus facile de travailler avec Atom que Vim ou Emacs. Ceci dit, si vous y tenez vraiment…)
3. Initiez un dépôt git, reliez le à un projet Framagit (ça devrait être un réflexe)
4. Créez un fichier appelé index.php et mettez y le contenu suivant :
    <?php
        echo "Hello World !";
    ?>
5. Affichez le fichier dans votre navigateur. Vous avez réalisé votre premier programme PHP !

### Exécuter vs Interprêter

On *exécute* un fichier *binaire* (langages de programmation compilés: C, C++, apk d’Android etc.)

On *interprête* un fichier *script* (langages de programmation scriptés: JavaScript, PHP, Python …)

## Variables

Les variables associent une valeur (inconnue) à un mot (la variable)  
En PHP, les variables se reconnaissence aisément : elles sont précédées d’un $  
Notez le dans un coin de votre tête : PHP est un langage à *typage faible*

### Exercice niveau 0

Créez le script suivant et affichez le :

    <?php
        $maVariable = "Philippe";
        echo "hello " . $maVariable;
    ?>

Affichez la page PHP. Changez la valeur (*Philippe*) par d’autres mots, chiffres etc. c’est formidable

Ici, la variable ayant toujours la même valeur, son usage n’a aucun intérêt. On pourrait aussi bien faire un *echo "maVariable";*  
*"hello" . $maVariable* est une *concaténation*. On en recausera à l’avenir

### Exercice niveau 1

Nous allons utiliser la *méthode GET* du *protocole HTTP* pour récupérer une valeur transmise par l’utilisateur.  
Créez le script suivant:

    <?php
        $maVariable = $_GET["maVariable"];
        echo "hello " . $maVariable;
    ?>

Appelez le script suivant en ajoutant *?maVariable=Philippe* après le nom de la page (*script2.php?maVariable=Philippe*). Changez la valeur (*Philippe*) par d’autres mots, chiffres etc. C’est incroyable.

### Exercice niveau 2

Créez le script suivant :

    <?php
        $chiffre1 = $_GET["chiffre1"];
        $chiffre2 = $_GET["chiffre2"];
        $resultat = $chiffre1 + $chiffre2;
        echo "Résultat: " . $resultat;
    ?>

Appelez le script suivant en ajoutant *?chiffre1=3&chiffre2=5* après le nom de la page. Changez un peu les valeurs. C’est astro le petit robot

## Logique booléenne

George Bool, mathématicien anglais, a créé de nombreuses choses : jeux, film et surtout logique

La logique booléenne est simple : on teste une condition et cette dernière est *vraie* (*true*) ou *fausse* (*false*)

### Cas simples

Comment que ça marche ? Bah c’est instinctif

* 3 > 5 : faux (strictement supérieur à)
* 3 <= 3 : vrai (inférieur ou égal)
* "3" == 3 : vrai (comparaison, typage faible)
* "3" === 3 : faux (comparaison, typage fort)
* 5 != 3 : vrai (différent)

### Un peu de complexification

Allez on complexifie. On peut mixer avec des conditions *ou* et *et*.  
*ou* : OR, ||
*et* : ET, &&

* (3 > 5) OR (5 == "5") : vrai (l’un des deux est vrai)
* (3 > 5) AND (5 == "5") : faux (l’un des deux est faux)

etc.  

### Utilisation

Nous allons utiliser la structure de contrôle *if* qui vérifie une condition. Si elle est vraie, le code est exécuté. Si elle est fausse, le code contenu dans le *else* est exécuté

    <?php
        $chiffre = $_GET["chiffre"];
        if(($chiffre%2) == 0) {
            echo "Le chiffre est pair";
        }
        else {
            echo "Le chiffre est impair";
        }
    ?>

Il existe des fonctions qui peuvent renvoyer des valeurs booléennes. Par exemple *isset* qui vérifie si une variable existe (par exemple *is_set($_GET["chiffre1"]*)

Et une version améliorée :

    <?php
        if(isset($_GET["chiffre"])) {
            $chiffre = $_GET["chiffre"];
            if(($chiffre%2) == 0) {
                echo "Le chiffre est pair";
            }
            else {
                echo "Le chiffre est impair";
            }
        }
        else {
            echo "Veuillez indiquer un chiffre";
        }
    ?>
