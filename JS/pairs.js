var fillPairsTable = function() {
    var jsonFile = JSON.parse(this.responseText);
    var students = jsonFile.students;
    var names = jsonFile.names;
    // number of pairs to create
    var numberOfPairs = Math.ceil(students.length/3);

    console.log(students);
    for(var i=0;i<numberOfPairs;i++) {
        var studentGroup = [];

        // we take 3 random students by pairs (not so pairs obviously …)
        // splice is used to avoid having a student chosen twice or more
        var studentIndex1 = Math.floor(Math.random() * students.length);
        studentGroup.push(students[studentIndex1]);
        students.splice(studentIndex1,1);

        if(students.length > 0) {
            var studentIndex2 = Math.floor(Math.random() * students.length);
            studentGroup.push(students[studentIndex2]);
            students.splice(studentIndex2,1);
        }

        if(students.length > 0) {
            var studentIndex3 = Math.floor(Math.random() * students.length);
            studentGroup.push(students[studentIndex3]);
            students.splice(studentIndex3,1);
        }

        var groupNameIndex = Math.floor(Math.random() * names.length);
        var groupName = names[groupNameIndex];
        names.splice(groupNameIndex,1);

        console.log(groupName);
        console.log(studentGroup);

        // GCU: write directly HTML. There *must* be a cleaner way
        var pairTR = "\t\t<tr>";
        pairTR += "<th>" + groupName["firstname"] + "&nbsp;" + groupName["lastname"] + "</th>";
        studentGroup.forEach(function(student) {
            pairTR += "<td>" + student["firstname"] + "&nbsp;" + student["lastname"] + "</td>";

        });
        pairTR += "</tr>\n";

        // This is so filthy I got myself sick (or was it this awful coffee ?)
        document.getElementById("pairs-table").innerHTML += pairTR;

        // Save this …
        localStorage.setItem(
                groupName["firstname"] + "_" + groupName["lastname"],
                JSON.stringify(studentGroup)
                );
    }
}
